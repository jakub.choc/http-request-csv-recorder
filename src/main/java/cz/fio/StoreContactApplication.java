package cz.fio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreContactApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreContactApplication.class, args);
    }

}
