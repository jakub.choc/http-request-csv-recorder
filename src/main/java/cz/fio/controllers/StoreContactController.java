package cz.fio.controllers;

import cz.fio.dto.RecordsResponseDTO;
import cz.fio.services.StoreContact;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class StoreContactController {

    private final StoreContact storeContact;

    @GetMapping(path = "/contact")
    public ResponseEntity<RecordsResponseDTO> receivePostback(
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String email) {
        return ResponseEntity.ok(storeContact.saveRowIntoFile(firstName, lastName, email));
    }
}
