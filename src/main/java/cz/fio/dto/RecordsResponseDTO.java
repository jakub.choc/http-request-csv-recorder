package cz.fio.dto;

import java.util.List;

public record RecordsResponseDTO(
        List<String> records
) {
}
