package cz.fio.exceptions;

public class DuplicityRecordException extends RuntimeException {

    public DuplicityRecordException(String message) {
        super(message);
    }
}
