package cz.fio.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({
            DuplicityRecordException.class
    })
    public ResponseEntity<String> duplicityApiException(Exception e, WebRequest request) {
        return new ResponseEntity<>("Record all-ready exist", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({
            NullPointerException.class
    })
    public ResponseEntity<String> nullInputApiException(Exception e, WebRequest request) {
        return new ResponseEntity<>("Null or empty input for record", HttpStatus.BAD_REQUEST);
    }
}
