package cz.fio.services;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import cz.fio.dto.RecordsResponseDTO;
import cz.fio.exceptions.DuplicityRecordException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StoreContact {

	private static final String FILE_PATH = "/Users/jakubchoc/Downloads/test-javista/src/main/java/cz/fio/temp/contacts.csv";

	public RecordsResponseDTO saveRowIntoFile(String firstName, String lastName, String email) {
		var row = createRowRecord(firstName, lastName, email);
		var cvsRecords= readFile();
		var recordsForSave = addRecord(cvsRecords, row);
		writeData(recordsForSave);
		return convertToDto(recordsForSave);
	}

	private List<String[]> addRecord(List<String[]> records, String[] row) {
		for (var rowRecord : records) 	{
			if (Arrays.equals(rowRecord, row)) {
				throw new DuplicityRecordException("Input record all-ready exist");
			}
		}
		records.add(row);
		return records;
	}

	private List<String[]> readFile() {
		List<String[]> fileRecord = new ArrayList<>();
		try {
			CSVReader reader = new CSVReader(new FileReader(FILE_PATH));
			fileRecord = reader.readAll();
			reader.close();
		} catch (CsvException | IOException e) {
			e.printStackTrace();
		}
		return fileRecord;
	}

	private void writeData(List<String[]> records) {
		try {
			CSVWriter writer = new CSVWriter(new FileWriter(FILE_PATH));
			writer.writeAll(records);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String[] createRowRecord(String firstName, String lastName, String email) {
		if (firstName.isBlank() || lastName.isBlank() || email.isBlank()) {
			throw new NullPointerException();
		}
		return new String[]{firstName, lastName, email};
	}

	public RecordsResponseDTO convertToDto(List<String[]> records) {
		var response =  records.stream()
				.map(Arrays::toString)
				.map(e -> e.replaceAll("[\\[\\] ]", ""))
				.toList();
		return new RecordsResponseDTO(response);
	}
}
