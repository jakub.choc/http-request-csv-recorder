package cz.fio;

import cz.fio.dto.RecordsResponseDTO;
import cz.fio.services.StoreContact;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class StoreContentTest {

    @InjectMocks
    private StoreContact storeContact;

    @Test
    void givenListOfArraysWithBrackets_whenCallingConvertToDto_ThenShouldReturnListOfStringWithoutBracketsInDTO() {
        String[] row = {"Karel", "Novak", "karel.novak@email.cz"};
        String[] row1 = {"Jan", "Blazek", "jan.blazek@email.cz"};

        List<String[]> records = new ArrayList<>();
        records.add(row);
        records.add(row1);


        List<String> expected = new ArrayList<>();
        expected.add("Karel,Novak,karel.novak@email.cz");
        expected.add("Jan,Blazek,jan.blazek@email.cz");

        var actual = storeContact.convertToDto(records);
        Assertions.assertEquals(new RecordsResponseDTO(expected), actual);
    }

    @Test
    void givenInputs_whenCallingRowRecord_ThenShouldReturnArrayOfString() {
        Assertions.assertThrows(NullPointerException.class, () -> storeContact.createRowRecord("", "Novak", "karel.novak@email.cz"));
    }
}
